import logo from './logo.svg';
import './App.css';
import TestFile from './TestFile.react';
// import firebase from "firebase/app";
// import firebase from 'firebase';
// import * as firebase from 'Firebase';
import firebase from 'firebase/app';
import {
  FirebaseDatabaseProvider,
  FirebaseDatabaseNode,
  FirebaseDatabaseTransaction
} from "@react-firebase/database";
import "firebase/database";
import React from 'react';
import { ALL_TEAMS } from './Data'
import { TextField } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Alert from '@material-ui/lab/Alert';
import TeamsSelector from './TeamsSelector.react';
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
  },
  paper: {
    width: 600,
    height: 'auto',
    overflow: 'auto',
  },
  button: {
    margin: theme.spacing(0.5, 0),
  },
}));

function not(a, b) {
  return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a, b) {
  return a.filter((value) => b.indexOf(value) !== -1);
}


function NewBet(props) {

  const [stage, setStage] = React.useState('name');
  const [data, setData] = React.useState({});
  const [name, setName] = React.useState('');

  console.log('render, stage is ' + stage);
  console.log('render, name is ' + name);

  switch(stage) {
    case 'name':
      return (
        <div style={{position: 'absolute', top: '30%', left:'40%'}}>
          <IconButton onClick={props.onFinished}>
                <ArrowBackIcon />
          </IconButton>
          <TextField
              id="standard-helperText"
              label="Name"
              required={true}
              defaultValue={name}
              placeholder="Your name"
              onChange={event => setName(event.target.value)}
          />
          <Button
                    variant="outlined"
                    size="small"
                    onClick={() => {
                            setStage('eighth');
                        }
                    }
                    disabled={name.length === 0}
                    aria-label="Save"
                >
                Save
            </Button>
        </div>
      );
    case 'eighth':
      return (
        <TeamsSelector teams={ALL_TEAMS} nextStageCount={16} exit={props.onFinished} title='Second Round' key='eighth' onSelected={newData => {
          console.log('onselected eigth');
          setData({...data, eighth: newData});
          setStage('quarter');
        }}/>
      );
    case 'quarter':
      return (
        <TeamsSelector teams={data.eighth} nextStageCount={8} exit={props.onFinished} title='Quarter Finals' key='quarter' onSelected={newData => {
          setData({...data, quarter: newData});
          setStage('half');
        }}/>
      );
    case 'half':
      return (
        <TeamsSelector teams={data.quarter} nextStageCount={4} exit={props.onFinished} title='Semi Finals' key='half' onSelected={newData => {
          setData({...data, half: newData});
          setStage('final');
        }}/>
      );
    case 'final':
      return (
        <TeamsSelector teams={data.half} nextStageCount={2} exit={props.onFinished} title='Finals' key='final' onSelected={newData => {
          setData({...data, final: newData});
          setStage('winner');
        }}/>
      );
    case 'winner':
      return (
        <TeamsSelector teams={data.final} nextStageCount={1} exit={props.onFinished} title='Winner' key='winner' onSelected={newData => {
          setData({...data, winner: newData});
          setStage('save');
        }}/>
      );
    case 'save':
      firebase.database().ref("/bets").get().then((snapshot) => {
            let bets = {};
            if (snapshot.exists()) {
                bets = snapshot.val();
            }

            bets[name] = data;
            firebase.database().ref("/bets").set(bets);
        }
      );
    case 'done':
      props.onFinished();
      return (
        <div/>
      );
  }
}

export default NewBet;
