import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import firebase from 'firebase/app';

var firebaseConfig = {
    apiKey: "AIzaSyAncz2PIDwHlJIgeND4h7U1IN1FrzSS0M0",
    authDomain: "euro-2021-friendly-bet.firebaseapp.com",
    databaseURL: "https://euro-2021-friendly-bet-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "euro-2021-friendly-bet",
    storageBucket: "euro-2021-friendly-bet.appspot.com",
    messagingSenderId: "520883007626",
    appId: "1:520883007626:web:e17e4fff105b83c167bd73",
    measurementId: "G-H4WKX9X647"
  };

firebase.initializeApp(firebaseConfig);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
