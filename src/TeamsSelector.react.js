import React from 'react';
import { TextField } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Alert from '@material-ui/lab/Alert';
import { ALL_TEAMS_WITH_ODDS } from './Data'
import Typography from '@material-ui/core/Typography';
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
  },
  paper: {
    width: 600,
    height: 'auto',
    overflow: 'auto',
  },
  button: {
    margin: theme.spacing(0.5, 0),
  },
}));

function not(a, b) {
  return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a, b) {
  return a.filter((value) => b.indexOf(value) !== -1);
}

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function selectRandomK(arr2, k) {
  const arr = arr2.slice();
  const result = [];
  for (let i = 0; i < k; i++) {
    const rand = getRandomInt(k - i);
    const next = arr[rand];
    arr[rand] = arr[arr.length - i - 1];
    result.push(next);
  }

  return result;
}


function TeamsSelector(props) {
    const classes = useStyles();
    const [checked, setChecked] = React.useState([]);
    const [left, setLeft] = React.useState(props.teams);
    const [right, setRight] = React.useState([]);
    const [error, setError] = React.useState(null);

    const leftChecked = intersection(checked, left);
    const rightChecked = intersection(checked, right);

    const handleToggle = (value) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };

    const handleCheckedRight = () => {
        if (right.length + leftChecked.length > props.nextStageCount) {
            setError(<Alert severity="error">{`Only ${props.nextStageCount} teams are allowed in next stage`}</Alert>);
            return;
        }
        setError(null);
        setRight(right.concat(leftChecked));
        setLeft(not(left, leftChecked));
        setChecked(not(checked, leftChecked));
    };

    const handleCheckedLeft = () => {
        setError(null);
        setLeft(left.concat(rightChecked));
        setRight(not(right, rightChecked));
        setChecked(not(checked, rightChecked));
    };

    const customList = (items) => (
        <Paper className={classes.paper}>
        <List dense component="div" role="list">
            {items.map((value) => {
            const labelId = `transfer-list-item-${value}-label`;

            return (
                <ListItem key={value} role="listitem" button onClick={handleToggle(value)}>
                <ListItemIcon>
                    <Checkbox
                        checked={checked.indexOf(value) !== -1}
                        tabIndex={-1}
                        disableRipple
                        inputProps={{ 'aria-labelledby': labelId }}
                    />
                </ListItemIcon>
                <ListItemText id={labelId} primary={value + " (" + ALL_TEAMS_WITH_ODDS[value] + " points)"} />
                </ListItem>
            );
            })}
            <ListItem />
        </List>
        </Paper>
    );

    const teamsLeft = props.nextStageCount - leftChecked.length - right.length;
    const teamsLeftString =
        teamsLeft === 0
            ? "You selected the right amount of teams"
            : (
                teamsLeft > 0
                    ? 'Select ' + teamsLeft + ' more team' + (teamsLeft === 1 ? '' : 's')
                    : 'You selected too many teams');

    return (
        <div>
            <IconButton onClick={props.exit}>
                <ArrowBackIcon />
            </IconButton>
            <div style={{margin: '20px'}}>
                <Typography variant="h2" component="h2" gutterBottom>
                    {props.title}
                </Typography>
                <Typography variant="h3" component="h3" gutterBottom>
                    {teamsLeftString}
                </Typography>
            </div>
            <Grid container spacing={2} justify="center" alignItems="center" className={classes.root}>
            {error}
            <Grid item key={props.key}>{customList(left)}</Grid>
            <Grid item>
                <Grid container direction="column" alignItems="center">
                <Button
                    variant="outlined"
                    size="small"
                    className={classes.button}
                    onClick={handleCheckedRight}
                    disabled={leftChecked.length === 0 || right.length >= props.nextStageCount}
                    aria-label="move selected right"
                >
                    &gt;
                </Button>
                <Button
                    variant="outlined"
                    size="small"
                    className={classes.button}
                    onClick={handleCheckedLeft}
                    disabled={rightChecked.length === 0}
                    aria-label="move selected left"
                >
                    &lt;
                </Button>
                <Button
                    variant="outlined"
                    size="small"
                    className={classes.button}
                    onClick={() => {
                        setChecked([]);
                        const newRight = selectRandomK(props.teams, props.nextStageCount);
                        setRight(newRight);
                        setLeft(props.teams.filter(team => !newRight.includes(team)));
                    }}
                    aria-label="move selected left"
                >
                    ווינרמט
                </Button>
                </Grid>
                </Grid>
                <Grid item>{customList(right)}</Grid>
                <Button
                    variant="outlined"
                    size="small"
                    className={classes.button}
                    onClick={() => {
                            props.onSelected(right);
                        }
                    }
                    disabled={right.length !== props.nextStageCount}
                    aria-label="Save"
                >
                Save
            </Button>
            </Grid>

        </div>
    );

}

export default TeamsSelector;
