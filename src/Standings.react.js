import logo from './logo.svg';
import './App.css';
import TestFile from './TestFile.react';
// import firebase from "firebase/app";
// import firebase from 'firebase';
// import * as firebase from 'Firebase';
import firebase from 'firebase/app';
import {
  FirebaseDatabaseProvider,
  FirebaseDatabaseNode,
  FirebaseDatabaseTransaction
} from "@react-firebase/database";
import "firebase/database";
import React from 'react';
import { LEVEL_MULTIPLIERS, ALL_TEAMS_WITH_ODDS } from './Data'
import { TextField } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Alert from '@material-ui/lab/Alert';
import TeamsSelector from './TeamsSelector.react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Link from '@material-ui/core/Link';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonBet from './PersonBet.react';
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import blue from '@material-ui/core/colors/blue';


const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
  },
  paper: {
    width: 600,
    height: 'auto',
    overflow: 'auto',
  },
  button: {
    margin: theme.spacing(0.5, 0),
  },
}));

function not(a, b) {
  return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a, b) {
  return a.filter((value) => b.indexOf(value) !== -1);
}


function Standings(props) {
  const classes = useStyles();
    const [results, setResults] = React.useState();
    const [name, setName] = React.useState();
    if (results == null) {
      firebase.database().ref("/bets").get().then((snapshotBets) => {
          const bets = snapshotBets.val();
          firebase.database().ref("/real").get().then((snapshotReal) => {
              const real = Object.fromEntries(Object.entries(snapshotReal.val()).map(([stage, realStageBet]) => [stage, new Set(realStageBet)]));
              firebase.database().ref("/cant_win").get().then((snapshotCantWin) => {
              const cantWin = snapshotCantWin.val();
              const scores = [];

              function add(accumulator, a) {
                return accumulator + a;
              }

              // bets['real'] = snapshotReal.val();

              for (const [name, personBet] of Object.entries(bets)) {
                  let personScore = 0;
                  for (const [stage, stageBet] of Object.entries(personBet)) {
                      const realStageBet = real[stage];
                      if (realStageBet == null) {
                        continue;
                      }
                      for (const team of stageBet) {
                          if (realStageBet.has(team)) {
                              personScore += (ALL_TEAMS_WITH_ODDS[team] ?? 0) * LEVEL_MULTIPLIERS[stage];
                          }
                      }
                  }
                  scores.push({name: name, score: personScore, cantWin: cantWin.includes(name)});
              }
              scores.sort((a,b) => b.score - a.score)
              setResults(scores);
          });
        });
      });
    }

    const dialog = name == null ? null : (
      <Dialog onClose={() => setName(null)} aria-labelledby="simple-dialog-title" open={name != null}>
        <DialogTitle id="simple-dialog-title">{name + "'s bet"}</DialogTitle>
        <PersonBet name={name}/>
    </Dialog>
    );

    const table = results == null ? null : (
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow style={{background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8EFF 90%)'}}>
              <TableCell>Name</TableCell>
              <TableCell align="left">Points</TableCell>
              <TableCell align="left">Can win?</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {results.map((result, i) => (
              <TableRow key={result.name} style={{background: (((i % 2) == 0) ? 'linear-gradient(45deg, #FFF00F 30%, #FFFFFF 90%)' : 'linear-gradient(45deg, #00FFFF 30%, #FFFFFF 90%)')}}>
                <TableCell component="th" scope="row">
                  <Link
                    key={name}
                    component="button"
                    onClick={() => {
                      setName(result.name);
                    }}
                  >
                    {result.name}
                  </Link>
                </TableCell>
                <TableCell align="left">{result.score}</TableCell>
                <TableCell align="left">{result.cantWin ? "No" : "Yes"}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );

    return (
    <div>
      <IconButton onClick={props.onFinished}>
        <ArrowBackIcon />
      </IconButton>
      <Backdrop className={classes.backdrop} open={results == null}>
        <CircularProgress color="inherit" />
      </Backdrop>
      {table}
      {dialog}
    </div>);
}

export default Standings;
