import logo from './logo.svg';
import './App.css';
import TestFile from './TestFile.react';
// import firebase from "firebase/app";
// import firebase from 'firebase';
// import * as firebase from 'Firebase';
import firebase from 'firebase/app';
import {
  FirebaseDatabaseProvider,
  FirebaseDatabaseNode,
  FirebaseDatabaseTransaction
} from "@react-firebase/database";
import "firebase/database";
import React from 'react';
import { LEVEL_MULTIPLIERS, ALL_TEAMS_WITH_ODDS } from './Data'
import { TextField } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Alert from '@material-ui/lab/Alert';
import TeamsSelector from './TeamsSelector.react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
  },
  paper: {
    width: 600,
    height: 'auto',
    overflow: 'auto',
  },
  button: {
    margin: theme.spacing(0.5, 0),
  },
}));

const order = {
  eighth: 0,
  quarter: 1,
  half: 2,
  final: 3,
  winner: 4,
}

function PersonBet(props) {
    const classes = useStyles();

    const [bet, setBet] = React.useState();
    if (bet == null) {
      if (props.name === 'real') {
        firebase.database().ref("/real").get().then((snapshotBets) => {
          const bet = snapshotBets.val();
          const betEntries = Object.entries(bet);
          betEntries.sort((a, b) => order[a[0]] - order[b[0]]);
          setBet(betEntries);
        });

      } else {
        firebase.database().ref("/bets").get().then((snapshotBets) => {
          const bets = snapshotBets.val();
          const betEntries = Object.entries(bets[props.name]);
          betEntries.sort((a, b) => order[a[0]] - order[b[0]]);
          setBet(betEntries);
        });
      }
    }

    let rows = [];
    if (bet != null) {
      for (let i = 0; i < 16; i++) {
        rows.push(bet.map(([_, teams]) => (<TableCell align="left">{teams.length >= i ? teams[i] : ''}</TableCell>)));
      }
    }

    const table = bet == null ? null : (<TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              {bet.map(([stage, _]) => (<TableCell>{stage}</TableCell>))}
            </TableRow>
          </TableHead>
          <TableBody>
          {rows.map(row => (<TableRow>{row}</TableRow>))}
          </TableBody>
        </Table>
      </TableContainer>);

    return (
    <div>
      <Backdrop className={classes.backdrop} open={bet == null}>
        <CircularProgress color="inherit" />
      </Backdrop>
      {table}
    </div>);

}

export default PersonBet;
