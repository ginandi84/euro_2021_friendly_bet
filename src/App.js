import logo from './logo.svg';
import './App.css';
import TestFile from './TestFile.react';
// import firebase from "firebase/app";
// import firebase from 'firebase';
// import * as firebase from 'Firebase';
import firebase from 'firebase/app';
import {
  FirebaseDatabaseProvider,
  FirebaseDatabaseNode,
  FirebaseDatabaseTransaction
} from "@react-firebase/database";
import "firebase/database";
import React from 'react';
import { ALL_TEAMS } from './Data'
import { TextField } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Alert from '@material-ui/lab/Alert';
import TeamsSelector from './TeamsSelector.react';
import NewBet from './NewBet.react';
import Standings from './Standings.react';


// var firebase = require('firebase');

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
  },
  paper: {
    width: 600,
    height: 'auto',
    overflow: 'auto',
  },
  button: {
    margin: theme.spacing(0.5, 0),
  },
}));

function not(a, b) {
  return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a, b) {
  return a.filter((value) => b.indexOf(value) !== -1);
}


function App() {
  const [display, setDisplay] = React.useState('standings');

  switch(display) {
    case 'intro':
      return (
        <div style={{position: 'absolute', top: '30%', left:'40%'}}>
        <Button
                variant="outlined"
                size="small"
                onClick={() => setDisplay('new_bet')}
                aria-label="New Bet"
            >
            New Bet
        </Button>
        <Button
               variant="outlined"
               size="small"
               onClick={() => setDisplay('standings')}
               aria-label="Standings"
            >
            Standings
        </Button>

        </div>
      );
    case 'new_bet':
      return (<NewBet onFinished={() => setDisplay('intro')}/>);
    case 'standings':
      return (<Standings onFinished={() => setDisplay('intro')}/>);

  }
}

export default App;
